### First time Setup

##### 1- Install dependency ( Windows )

```
choco install nodejs-lts # From an elevated prompt
node --version # validate it's latest LTS version.
npx playwright install # Will install chromium, firefox, webkit
```
- https://community.chocolatey.org/packages/nodejs-lts#install
- Install Package manager chocolatey `https://chocolatey.org/install`

### Running tests
> Before running the test:
- Make sure to remove all tasks for Ms Betty Bair.
- Make sure to remove all saved filter.

```
npm i
npx playwright test --workers 1 --project chromium
```
> Note: Test must be executed one at a time in sequence.


### Futur / possible improvement
- Add logging to ease the maintenance of the script and stop test early if some conditions are not met.
- Improve choice of selector:
    - Usage of Applitools to find some element visually.
    - Use more precise selector with XPATH.
- Bypassing login page using cookie (so you don't have to repeat the login step every time)
- Automate the setup of the test with salesforce API. ( i.e crud filter / task / people ,.. etc )
- Add parallelism.
- Use Mock Api to have a better control over the network.
- Enhance test speed and organization with a better understanding of the business case.


### Reference
- See Install playwright https://playwright.dev/docs/intro
