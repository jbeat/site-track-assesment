// @ts-check
const { test, expect } = require('@playwright/test');
const { MainPage } = require('./page-object/main-page');
const { LeadsPage } = require('./page-object/leads-page');
const { ActivityPage } = require('./page-object/activity-page');

test('Validate Leads Filter', async ({ page }) => {
  test.setTimeout(60000); // Set test timeout to 60 sec.
  const mainPage = new MainPage(page);
  await mainPage.login();
  await mainPage.clickOnAppLauncher();
  await mainPage.goToLeadsPageFromAppLauncher();

  const leadsPage = new LeadsPage(page);
  await leadsPage.showFilter();
  await leadsPage.clickOnAddFilter();
  await leadsPage.chooseCreatedDateFilter();
  await leadsPage.chooseCreatedGreaterOrEqualFilter();
  await leadsPage.fillCurrentDate();
  await leadsPage.clickOnSaveFilter();
  await leadsPage.closeFilter();

  const searchDescription = await leadsPage.getLeadTable();
  expect(searchDescription).toContain("22");
});

test('Validate Lead interaction and Task creation', async ({ page }) => {
  test.setTimeout(60000); // Set test timeout to 60 sec.
  const mainPage = new MainPage(page);
  await mainPage.login();
  await mainPage.clickOnAppLauncher();
  await mainPage.goToLeadsPageFromAppLauncher();

  const leadsPage = new LeadsPage(page);
  await leadsPage.showFilter();
  await leadsPage.clickOnAddFilter();
  await leadsPage.chooseCreatedDateFilter();
  await leadsPage.chooseCreatedGreaterOrEqualFilter();
  await leadsPage.fillCurrentDate();
  await leadsPage.clickOnSaveFilter();
  await leadsPage.closeFilter();

  await leadsPage.clickOnBettyBairLead();

  await leadsPage.createNewTask();
  await leadsPage.typeTaskTitle("Create Budget Plan");
  await leadsPage.insertCurrentTaskDate();
  await leadsPage.assignQaAdminIfNobodyIsAssign();
  await leadsPage.setTaskToInProgress();
  await leadsPage.saveTask();
  await leadsPage.checkPopupToValidateTaskCreation("Create Budget Plan");

  await leadsPage.createNewTask();
  await leadsPage.typeTaskTitle("Submit Budget Plan for Review");
  await leadsPage.insertNextWeekTaskDate();
  await leadsPage.saveTask();
  await leadsPage.checkPopupToValidateTaskCreation("Submit Budget Plan for Review");

  await page.reload({ waitUntil: 'load' });

  const activityPage = new ActivityPage(page);
  await activityPage.checkIfTasksWereCreated();
  await activityPage.checkDescriptionOfCreatedBudgetPlan();
  await activityPage.addDescriptionBudgetForQ4ToCreatedBudgetPlan();
  await activityPage.checkDescriptionOfCreatedBudgetPlanForBudgetForQ4();
  await activityPage.clickOnTimelineSettings();
  await activityPage.clickOnDateRangeNext7Days();
  await activityPage.applyAndSaveActivityFilter();
  await activityPage.checkIfTasksWereCreated();
});