const { expect } = require('@playwright/test');
const date = require('date-and-time');

exports.LeadsPage = class LeadsPage {

  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
    this.showFilterBtn = this.page.getByTitle("Show filters");
    this.addFilterBtn = this.page.getByText("Add Filter").nth(0);
    this.filterTypeBtn = this.page.locator('[data-value="Annual Revenue"]');
    this.dateValueBtn = this.page.locator('[data-value="equals"]')
    this.inputDateValueForFilter = this.page.locator('[class="filterTextInput valueInput input uiInput uiInputText uiInput--default uiInput--input"]');
    this.doneFilterBtn = this.page.getByText("Done");
    this.saveFilterBtn = this.page.locator('[class="slds-button slds-button_brand saveButton headerButton"]');
    this.closeFilterBtn = this.page.getByTitle("Close Filters");
    this.bettyHairLink = this.page.locator('[href="/lightning/r/Lead/00Qak000005ntnCEAQ/view"]');
    this.createNewtaskBtn = this.page.getByTitle("New Task");
    this.taskTitleInput = this.page.locator('[class="slds-form form-horizontal slds-is-editing"]').locator("input").nth(0);
    this.taskDateInput = this.page.locator('[class="slds-form form-horizontal slds-is-editing"]').locator("input").nth(1);
    this.taskAssignInput = this.page.getByPlaceholder("Search People...");
    this.taskQaAdminAssignee = this.page.getByTitle("QA Admin");
    this.taskStatusInput = this.page.getByText("Not Started");
    this.saveTaskBtn = this.page.getByText("Save").nth(3);
    this.leadTable = this.page.locator('[class="countSortedByFilteredBy"]');
  }

  async showFilter() {
    await this.showFilterBtn.click();
  }

  async clickOnAddFilter() {
    await this.addFilterBtn.click();
  }

  async chooseCreatedDateFilter() {
    await this.filterTypeBtn.click();
    await this.page.keyboard.type("Cre", { delay: 200 });
    await this.page.keyboard.press("Enter");
    await this.page.waitForTimeout(500); // Slow down the Tests aon purpose to sync UI.
  }

  async chooseCreatedGreaterOrEqualFilter() {
    await this.dateValueBtn.click();
    await this.page.keyboard.type("grea", { delay: 200 });
    await this.page.keyboard.press("Enter");
  }

  async fillCurrentDate() {
    await this.inputDateValueForFilter.fill(this.getCurrentDate());
    await this.doneFilterBtn.click();
  }

  async clickOnSaveFilter() {
    await this.saveFilterBtn.click();
  }

  async closeFilter() {
    await this.closeFilterBtn.click();
  }

  async clickOnBettyBairLead() {
    await this.bettyHairLink.click();
  }

  async createNewTask() {
    await this.createNewtaskBtn.click();
  }

  async typeTaskTitle(title) {
    await this.taskTitleInput.click();
    await this.page.keyboard.type(`${title}`, { delay: 200 });
  }

  async insertCurrentTaskDate() {
    await this.taskDateInput.click();
    await this.page.keyboard.type(this.getCurrentDate(), { delay: 200 });
  }

  async insertNextWeekTaskDate() {
    await this.taskDateInput.click();
    await this.page.keyboard.type(this.getNextWeek(), { delay: 200 });
  }

  async assignQaAdminIfNobodyIsAssign() {
    const assign = await this.taskAssignInput.isHidden();
    if (!assign) {
      await this.taskAssignInput.click();
      await this.page.keyboard.type("QA Admin", { delay: 200 });
      await this.taskQaAdminAssignee.click();
    }
  }

  async setTaskToInProgress() {
    await this.taskStatusInput.click();
    await this.page.keyboard.type("i", { delay: 200 });
    await this.page.keyboard.press("Enter");
  }

  async saveTask() {
    await this.saveTaskBtn.click();
  }

  async checkPopupToValidateTaskCreation(taskname) {
    await this.page.getByText(`Task "${taskname}" was created.`).isVisible();
  }

  async getLeadTable() {
    return await this.leadTable.textContent();
  }

  getCurrentDate() {
    const now = new Date();
    return date.format(now, 'MM/DD/YYYY');
  }

  getNextWeek() {
    const now = new Date();
    now.setDate(now.getDate() + 7)
    return date.format(now, 'MM/DD/YYYY');
  }
};