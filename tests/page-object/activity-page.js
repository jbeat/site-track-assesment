exports.ActivityPage = class ActivityPage {

  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
    this.chevronForCreatedForBudgetPlanActivity = this.page.locator('[class="chevronLink"]').nth(1);
    this.descriptionForActivity = this.page.getByTitle("Description");
    this.activityContextualMenuBtn = this.page.locator('[class="rowActionsPlaceHolder slds-button slds-button--icon-x-small slds-button_icon-x-small slds-button--icon-border-filled slds-button_icon-border-filled keyboardMode--trigger"]').nth(1);
    this.timeLineSettingBtn = this.page.getByText("Timeline Settings");
    this.applyAndSaveFilterActivity = this.page.getByText("Apply & Save");
    this.checkBox7days = this.page.locator('[class="radioFaux slds-radio_faux slds-var-m-right_x-small"]').nth(1);
    this.saveButtonForEditDescription = this.page.locator('[class="slds-button slds-button_brand cuf-publisherShareButton undefined uiButton"]');
  }

  async checkIfTasksWereCreated() {
    await this.page.getByText("Create Budget Plan", { exact: true }).nth(1).isVisible();
    await this.page.getByText("Submit Budget Plan for Review", { exact: true }).nth(1).isVisible();
  }

  async checkDescriptionOfCreatedBudgetPlan() {
    await this.chevronForCreatedForBudgetPlanActivity.click();
    await this.descriptionForActivity.isVisible();
  }

  async addDescriptionBudgetForQ4ToCreatedBudgetPlan() {
    await this.activityContextualMenuBtn.click();
    await this.page.getByText("Edit Comments").click();
    await this.page.locator('textarea').click();
    await this.page.waitForTimeout(500); // Slow down the test on purpose.
    await this.page.keyboard.type("Budget for Q4", { delay: 200 });
    await this.saveButtonForEditDescription.click();
  }

  async checkDescriptionOfCreatedBudgetPlanForBudgetForQ4() {
    await this.chevronForCreatedForBudgetPlanActivity.click();
    await this.page.getByText("Budget for Q4").isVisible();
  }

  async clickOnTimelineSettings() {
    await this.timeLineSettingBtn.click();
  }

  async applyAndSaveActivityFilter() {
    await this.applyAndSaveFilterActivity.click();
  }

  async clickOnDateRangeNext7Days() {
    await this.checkBox7days.click();
  }

};