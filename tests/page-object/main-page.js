const { expect } = require('@playwright/test');

exports.MainPage = class MainPage {

  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
    this.getUsername = this.page.locator("#username");
    this.getPassword = this.page.locator("#password");
    this.getLoginBtn = this.page.locator("#Login");
    
    this.SALESFORCE_LOGIN_URL = 'https://sitetracker-1a-dev-ed.develop.my.salesforce.com'
    this.USERNAME = "qa-auto@sitetracker.com"
    this.PASSWORD = "Test123$"

    this.appLauncherBtn = this.page.getByTitle("App Launcher");
    this.searchAppsAndItemsInput = this.page.getByPlaceholder("Search apps and items...");
    this.leadsPageBtn = this.page.locator("#Lead");
  }

  async login() {
    await this.page.goto(this.SALESFORCE_LOGIN_URL);
    await this.getUsername.fill(this.USERNAME);
    await this.getPassword.fill(this.PASSWORD);
    await this.getLoginBtn.click();
  }

  async clickOnAppLauncher() {
    await this.appLauncherBtn.click();
  }

  async goToLeadsPageFromAppLauncher() {
    await this.searchAppsAndItemsInput.fill("Leads");
    await this.leadsPageBtn.click();
  }

};